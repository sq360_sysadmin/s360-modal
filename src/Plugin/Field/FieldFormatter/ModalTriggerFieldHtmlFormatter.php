<?php

namespace Drupal\s360_modal\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'modal_trigger_field' formatter.
 *
 * @FieldFormatter(
 *   id = "modal_trigger_field_html",
 *   label = @Translation("HTML Markup"),
 *   field_types = {
 *     "modal_trigger_field"
 *   }
 * )
 */
class ModalTriggerFieldHtmlFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#item' => $item,
        '#theme' => 'modal_trigger_field',
      ];
    }

    return $elements;
  }

}
