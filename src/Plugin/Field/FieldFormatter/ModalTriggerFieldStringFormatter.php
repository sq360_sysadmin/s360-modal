<?php

namespace Drupal\s360_modal\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'modal_trigger_field' formatter.
 *
 * @FieldFormatter(
 *   id = "modal_trigger_field_string",
 *   label = @Translation("Plain text"),
 *   field_types = {
 *     "modal_trigger_field"
 *   }
 * )
 */
class ModalTriggerFieldStringFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewValue($item);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  private function viewValue(FieldItemInterface $item) {
    return [
      '#type' => 'inline_template',
      '#template' => '{{ value|nl2br }}',
      '#context' => [
        'value' => "$item->label (Modal Id: $item->modal_id)",
      ],
    ];
  }

}
