<?php

namespace Drupal\s360_modal\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'modal_options_field' formatter.
 *
 * @FieldFormatter(
 *   id = "modal_options_field_html_list",
 *   label = @Translation("HTML List"),
 *   field_types = {
 *     "modal_options_field"
 *   }
 * )
 */
class ModalOptionsFieldHtmlListFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewValue($item);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  private function viewValue(FieldItemInterface $item) {
    $display_options = $item->display_options;
    $trigger_options = $item->trigger_options;
    $modal_options = array_merge($display_options, $trigger_options);

    $trigger = $modal_options['trigger'];

    $dl_children = [];

    if ($trigger === 'manual') {
      unset($modal_options['timer_delay']);
      unset($modal_options['scroll_delay']);
      unset($modal_options['frequency']);
      unset($modal_options['banish_text']);
    }
    elseif (
      $trigger === 'scroll' or
      $trigger === 'exit_behaviour'
    ) {
      unset($modal_options['timer_delay']);
    }
    elseif (
      $trigger == 'timer' or
      $trigger == 'exit_behaviour'
    ) {
      unset($modal_options['scroll_delay']);
    }

    foreach ($modal_options as $key => $modal_option) {
      if ($modal_option !== '') {
        $dl_children[] = [
          [
            '#type' => 'html_tag',
            '#tag' => 'dt',
            '#value' => ucwords(str_replace('_', ' ', $key)),
          ],
          [
            '#type' => 'html_tag',
            '#tag' => 'dd',
            '#value' => $modal_option,
          ],
        ];
      }
    }

    return [
      '#type' => 'html_tag',
      '#tag' => 'dl',
      'child' => [
        $dl_children,
      ],
    ];
  }

}
