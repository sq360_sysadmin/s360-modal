<?php

namespace Drupal\s360_modal\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Plugin implementation of the 'modal_options_field' field type.
 *
 * @FieldType(
 *   id = "modal_options_field",
 *   label = @Translation("Modal Options Field"),
 *   description = @Translation("Modal Options Field"),
 *   category = @Translation("Modal"),
 *   default_widget = "modal_options_field",
 *   default_formatter = "modal_options_field_html_list",
 *   cardinality = 1,
 * )
 */
class ModalOptionsFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'styles' => '',
      'positions' => implode("\r\n", [
        'frame-top|Frame (Top)',
        'frame-bottom|Frame (Bottom)',
        'popup-small|Popup (Small)',
        'popup-medium|Popup (Medium)',
        'popup-large|Popup (Large)',
        'popup-xlarge|Popup (Extra Large)',
        'side-top-right|Side (Top Right)',
        'side-top-left|Side (Top Left)',
        'side-bottom-right|Side (Bottom Right)',
        'side-bottom-left|Side (Bottom Left)',
        'fullscreen|Curtain (Fullscreen)',
      ]),
      'timer_delays' => implode("\r\n", [
        '5|After 5 seconds',
        '10|After 10 seconds',
        '15|After 15 seconds',
      ]),
      'scroll_delays' => implode("\r\n", [
        '25|25% Down',
        '50|50% Down',
        '75|75% Down',
      ]),
      'frequencies' => implode("\r\n", [
        '3600|1 Hour',
        '28800|8 Hours',
        '86400|1 Day',
      ]),
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'display_options' => [
          'description' => 'Serialized array of display options for the modal.',
          'type' => 'blob',
          'size' => 'big',
          'serialize' => TRUE,
        ],
        'trigger_options' => [
          'description' => 'Serialized array of trigger options for the modal.',
          'type' => 'blob',
          'size' => 'big',
          'serialize' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['display_options'] = MapDataDefinition::create()
      ->setLabel(t('Display Options'));

    $properties['trigger_options'] = MapDataDefinition::create()
      ->setLabel(t('Trigger Options'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $default_description = implode("<br>", [
      $this->t('The key is the stored value. The label will be used in displayed values and edit forms.'),
      $this->t('Enter one value per line, in the format <strong>key|label</strong>.'),
    ]);

    $element['styles'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#default_value' => $this->getSetting('styles'),
      '#title' => $this->t('Styles'),
      '#description' => '<p>' . implode('', [
        $default_description,
      ]) . '</p>',
    ];

    $element['positions'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#default_value' => $this->getSetting('positions'),
      '#title' => $this->t('Positions'),
      '#required' => TRUE,
      '#description' => '<p>' . implode('', [
        $default_description,
      ]) . '</p>',
    ];

    $element['timer_delays'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#default_value' => $this->getSetting('timer_delays'),
      '#title' => $this->t('Timer Delays'),
      '#required' => TRUE,
      '#description' => '<p>' . implode('<br>', [
        $default_description,
        $this->t('Key must be in seconds.'),
      ]) . '</p>',
    ];

    $element['scroll_delays'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#default_value' => $this->getSetting('scroll_delays'),
      '#title' => $this->t('Scroll Delays'),
      '#required' => TRUE,
      '#description' => '<p>' . implode('<br>', [
        $default_description,
      ]) . '</p>',
    ];

    $element['frequencies'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#default_value' => $this->getSetting('frequencies'),
      '#title' => $this->t('Frequencies'),
      '#required' => TRUE,
      '#description' => '<p>' . implode('<br>', [
        $default_description,
        $this->t('Key must be in seconds.'),
      ]) . '</p>',
    ];

    return $element;
  }

}
