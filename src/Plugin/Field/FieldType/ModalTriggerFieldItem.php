<?php

namespace Drupal\s360_modal\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Plugin implementation of the 'modal_trigger_field' field type.
 *
 * @FieldType(
 *   id = "modal_trigger_field",
 *   label = @Translation("Modal Trigger Field"),
 *   description = @Translation("Modal Trigger Field"),
 *   category = @Translation("Modal"),
 *   default_widget = "modal_trigger_field",
 *   default_formatter = "modal_trigger_field_html",
 *   cardinality = 1,
 * )
 */
class ModalTriggerFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'styles' => '',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'label' => DRUPAL_REQUIRED,
      'modal_id' => DRUPAL_REQUIRED,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'label' => [
          'description' => 'Label for the modal trigger button.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'modal_id' => [
          'description' => 'Id of the modal to trigger.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'options' => [
          'description' => 'Serialized array of options for the link.',
          'type' => 'blob',
          'size' => 'big',
          'serialize' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['label'] = DataDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE);

    $properties['modal_id'] = DataDefinition::create('string')
      ->setLabel(t('Modal Id'))
      ->setRequired(TRUE);

    $properties['options'] = MapDataDefinition::create()
      ->setLabel(t('Options'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element['styles'] = [
      '#type' => 'textarea',
      '#rows' => 10,
      '#default_value' => $this->getSetting('styles'),
      '#title' => $this->t('Custom Styles'),
      '#description' => $this->t('<p>Enter one value per line, in the format <strong>name|label</strong>.</p>'),
    ];

    return $element;
  }

}
