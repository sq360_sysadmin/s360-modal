<?php

namespace Drupal\s360_modal\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;

/**
 * Plugin implementation of the 'modal_trigger_field' widget.
 *
 * @FieldWidget(
 *   id = "modal_trigger_field",
 *   label = @Translation("Modal Trigger Field"),
 *   field_types = {
 *     "modal_trigger_field"
 *   }
 * )
 */
class ModalTriggerFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];

    $element['#attributes'] = [
      'class' => ['s360-modal-trigger-field'],
    ];
    $element['#type'] = 'fieldset';
    $element['#attached'] = [
      'library' => ['s360_modal/s360_modal.modal_trigger_field.admin'],
    ];

    if (count($this->getStyleOptions())) {
      $element['style'] = [
        '#title' => $this->t('Style'),
        '#type' => 'select',
        '#options' => $this->getStyleOptions(),
        '#empty_option' => $this->t('- Select -'),
        '#default_value' => $item->options['style'] ?? [],
        '#attributes' => [
          'class' => ['s360-modal-trigger-field__style'],
        ],
      ];
    }

    $element['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $item->label ?? '',
      '#attributes' => [
        'class' => ['s360-modal-trigger-field__label'],
      ],
    ];

    $element['modal_id'] = [
      '#title' => $this->t('Modal ID'),
      '#type' => 'textfield',
      '#default_value' => $item->modal_id ?? '',
      '#attributes' => [
        'class' => ['s360-modal-trigger-field__modal-id'],
      ],
    ];

    $element['#element_validate'][] = [get_called_class(), 'processFields'];
    $element['#element_validate'][] = [get_called_class(), 'processOptions'];

    return $element;
  }

  /**
   * Update field values.
   */
  public static function processFields(&$element, FormStateInterface $form_state, $form) {
    $values = $form_state->getValue($element['#parents']);

    $values['label'] = $element['label']['#value'];
    $values['modal_id'] = $element['modal_id']['#value'];

    $form_state->setValueForElement($element, $values);
  }

  /**
   * Copy custom values into the option field.
   */
  public static function processOptions(&$element, FormStateInterface $form_state, $form) {
    $values = $form_state->getValue($element['#parents']);

    $values['options'] = [
      'style' => isset($element['style']) ? $element['style']['#value'] : '',
    ];

    $form_state->setValueForElement($element, $values);
  }

  /**
   * Creates an array of available styles.
   *
   * @return array
   *   An associative array of available styles.
   */
  private function getStyleOptions() {
    $style_options = [];

    // Get the custom_styles and apply some function to normalize them.
    $custom_styles = explode("\n", $this->getFieldSetting('styles'));
    $custom_styles = array_map('trim', $custom_styles);
    $custom_styles = array_filter($custom_styles, 'strlen');

    foreach ($custom_styles as $custom_style) {
      $matches = [];

      if (preg_match('/(.*)\|(.*)/', $custom_style, $matches)) {
        $key = trim($matches[1]);
        $value = trim($matches[2]);
      }

      $style_options[$key] = $value;
    }

    return $style_options;
  }

}
