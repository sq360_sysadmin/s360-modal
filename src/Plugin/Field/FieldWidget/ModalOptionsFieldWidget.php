<?php

namespace Drupal\s360_modal\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;

/**
 * Plugin implementation of the 'modal_options_field' widget.
 *
 * @FieldWidget(
 *   id = "modal_options_field",
 *   label = @Translation("Modal Options Field"),
 *   field_types = {
 *     "modal_options_field"
 *   }
 * )
 */
class ModalOptionsFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];

    $element['#attributes'] = [
      'class' => ['s360-modal-options-field'],
    ];
    $element['#type'] = 'fieldset';
    $element['#attached'] = [
      'library' => ['s360_modal/s360_modal.modal_options_field.admin'],
    ];

    $element['display_group'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Display'),
      '#attributes' => [
        'class' => ['s360-modal-options-field__display-group'],
      ],
    ];

    $element['display_group']['position'] = [
      '#title' => $this->t('Position'),
      '#required' => TRUE,
      '#type' => 'select',
      '#options' => $this->getFieldSettingOptions('positions'),
      '#default_value' => $item->display_options['position'] ?? '',
    ];

    if (count($this->getFieldSettingOptions('styles'))) {
      $element['display_group']['style'] = [
        '#title' => $this->t('Style'),
        '#type' => 'select',
        '#options' => $this->getFieldSettingOptions('styles'),
        '#empty_option' => $this->t('- Select -'),
        '#default_value' => $item->display_options['style'] ?? '',
      ];
    }

    $element['trigger_group'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Trigger'),
      '#attributes' => [
        'class' => ['s360-modal-options-field__trigger-group'],
      ],
    ];

    $element['trigger_group']['trigger'] = [
      '#title' => $this->t('Trigger'),
      '#required' => TRUE,
      '#type' => 'select',
      '#description' => $this->t('Choose how this modal will be triggered to display. For manual triggers, please take note of the Modal ID displayed. You must save the modal with a manual trigger first in order to get a Modal Id to use for the trigger.'),
      '#options' => $this->getTriggerOptions(),
      '#default_value' => $item->trigger_options['trigger'] ?? '',
    ];

    $element['trigger_group']['timer_delay'] = [
      '#title' => $this->t('Timer'),
      '#type' => 'select',
      '#description' => $this->t('How long after the page loads that he modal istriggered to display.'),
      '#options' => $this->getFieldSettingOptions('timer_delays'),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $item->trigger_options['timer_delay'] ?? 5000,
      '#states' => [
        'visible' => [
          'select[name*="[trigger]"]' => ['value' => 'timer'],
        ],
        'required' => [
          'select[name*="[trigger]"]' => ['value' => 'timer'],
        ],
      ],
    ];

    $element['trigger_group']['scroll_delay'] = [
      '#title' => $this->t('Scroll Delay'),
      '#type' => 'select',
      '#description' => $this->t('How far down a page you scroll before triggering the modal.'),
      '#options' => $this->getFieldSettingOptions('scroll_delays'),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $item->trigger_options['scroll_delay'] ?? 25,
      '#states' => [
        'visible' => [
          'select[name*="[trigger]"]' => ['value' => 'scroll'],
        ],
        'required' => [
          'select[name*="[trigger]"]' => ['value' => 'scroll'],
        ],
      ],
    ];

    $element['trigger_group']['frequency'] = [
      '#title' => $this->t('Frequency of Repeat'),
      '#type' => 'select',
      '#description' => $this->t('How often a modal will re-display after being dismissed.'),
      '#options' => $this->getFrequencyOptions(),
      '#default_value' => $item->trigger_options['frequency'] ?? '',
      '#states' => [
        'visible' => [
          'select[name*="[trigger]"]' => ['!value' => 'manual'],
        ],
        'required' => [
          'select[name*="[trigger]"]' => ['!value' => 'manual'],
        ],
      ],
    ];

    $element['trigger_group']['banish_text'] = [
      '#title' => $this->t('Banish Text'),
      '#type' => 'textfield',
      '#description' => $this->t('If there is text in this field, it will show a button that will allow the user to <strong>permanently</strong> banish this modal. This text will show as a tool-tip and be read by screen readers.'),
      '#maxlength' => 128,
      '#default_value' => $item->trigger_options['banish_text'] ?? '',
      '#states' => [
        'visible' => [
          ['select[name*="[frequency]"]' => ['!value' => -1]],
          'or',
          ['select[name*="[trigger]"]' => ['!value' => 'manual']],
        ],
      ],
    ];

    $element['#element_validate'][] = [
      get_called_class(),
      'processDisplayOptions',
    ];

    $element['#element_validate'][] = [
      get_called_class(),
      'processTriggerOptions',
    ];

    return $element;
  }

  /**
   * Copy custom values into the display field.
   */
  public static function processDisplayOptions(&$element, FormStateInterface $form_state, $form) {
    $values = $form_state->getValue($element['#parents']);

    $values['display_options'] = [
      'position' => $element['display_group']['position']['#value'],
      'style' => isset($element['display_group']['style']) ? $element['display_group']['style']['#value'] : '',
    ];

    $form_state->setValueForElement($element, $values);
  }

  /**
   * Copy custom values into the option field.
   */
  public static function processTriggerOptions(&$element, FormStateInterface $form_state, $form) {
    $values = $form_state->getValue($element['#parents']);

    $values['trigger_options'] = [
      'trigger' => $element['trigger_group']['trigger']['#value'],
      'timer_delay' => isset($element['trigger_group']['timer_delay']) ? $element['trigger_group']['timer_delay']['#value'] : '',
      'scroll_delay' => isset($element['trigger_group']['scroll_delay']) ? $element['trigger_group']['scroll_delay']['#value'] : '',
      'frequency' => isset($element['trigger_group']['frequency']) ? $element['trigger_group']['frequency']['#value'] : '',
      'banish_text' => isset($element['trigger_group']['banish_text']) ? $element['trigger_group']['banish_text']['#value'] : '',
    ];

    $form_state->setValueForElement($element, $values);
  }

  /**
   * Converts a string of key|labels into an associative array.
   *
   * @param string $setting
   *   Which field setting options.
   *
   * @return array
   *   The associative array.
   */
  private function getFieldSettingOptions($setting) {
    $options = [];

    $field_setting_options = explode("\r\n", $this->getFieldSetting($setting));
    $field_setting_options = array_map('trim', $field_setting_options);
    $field_setting_options = array_filter($field_setting_options, 'strlen');

    foreach ($field_setting_options as $field_setting_option) {
      $matches = [];

      if (preg_match('/(.*)\|(.*)/', $field_setting_option, $matches)) {
        $key = trim($matches[1]);
        $value = trim($matches[2]);
      }

      $options[$key] = $value;
    }

    return $options;
  }

  /**
   * Returns an associative array of modal triggers.
   *
   * @return array
   *   Modal triggers.
   */
  private function getTriggerOptions() {
    return [
      'scroll' => 'On page scroll',
      'manual' => 'Manual',
      'timer' => 'After a specific time',
      'exit_behaviour' => 'Exit Behaviour',
    ];
  }

  /**
   * Returns an associative array of modal frequency.
   *
   * @return array
   *   Modal timer delays.
   */
  private function getFrequencyOptions() {
    $options = $this->getFieldSettingOptions('frequencies');

    $immediate_option = ['0' => $this->t('Immediately')];
    $never_option = ['-1' => $this->t('Never')];

    $options = $immediate_option + $options + $never_option;

    return $options;
  }

}
