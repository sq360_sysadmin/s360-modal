<?php

/**
 * @file
 * s360_modal.module
 */

use Drupal\Core\Template\Attribute;
use Drupal\s360_modal\Plugin\Field\FieldType\ModalTriggerFieldItem;

/**
 * Implements hook_theme().
 */
function s360_modal_theme($existing, $type, $theme, $path) {
  return [
    'modal_trigger_field' => [
      'render element' => 'elements',
    ],
    'modal' => [
      'template' => 'modal' ,
      'base hook' => 'paragraph',
    ],
    'block__modal' => [
      'template' => 'block--modal',
      'base hook' => 'block',
    ],
    'paragraph__modal_layout' => [
      'template' => 'paragraph--modal-layout' ,
      'base hook' => 'paragraph',
    ],
  ];
}

/**
 * Implements hook_preprocess_paragraph() for modal_layout.
 */
function s360_modal_preprocess_paragraph__modal_layout(&$variables) {
  $variables['content']['regions']['#layout_tag'] = 'div';
}

/**
 * Implements hook_theme_suggestions_block_alter().
 */
function s360_modal_theme_suggestions_paragraph_alter(array &$suggestions, array $variables) {
  // The admin theme should use the default paragraph.html.twig template.
  // This makes sure the suggestion is only applied when viewing the site.
  if (!\Drupal::service('router.admin_context')->isAdminRoute()) {
    $paragraph_bundle = $variables['elements']['#paragraph']->bundle();

    if ($paragraph_bundle === 'modal') {
      $suggestions[] = 'modal';
    }
  }
}

/**
 * Implements hook_theme_suggestions_block_alter().
 */
function s360_modal_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  if (isset($variables['elements']['content']['#block_content'])) {
    $block_bundle = $variables['elements']['content']['#block_content']->bundle();

    if ($block_bundle === 'modal') {
      $suggestions[] = 'block__modal';
    }
  }
}

/**
 * Implements hook_theme_preprocess_paragraph() for modal_preview.
 */
function s360_modal_preprocess_paragraph__modal__preview(array &$variables) {
  $elements = $variables['elements'];

  if (isset($elements['#paragraph'])) {
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $elements['#paragraph'];

    $variables['content']['field_modal_id'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#weight' => -100,
      '#attributes' => [
        'class' => [
          'field',
          'field--name-field-modal-id',
          'field--type-string',
          'field--label-inline',
        ],
      ],
      'child' => [
        [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => 'Modal Id',
          '#attributes' => [
            'class' => 'field__label',
          ],
        ],
        [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => '<strong>' . _s360_modal_get_modal_id($paragraph->id()) . '</strong>',
          '#attributes' => [
            'class' => 'field__item',
          ],
        ],
      ],
    ];
  }
}

/**
 * Prepares variables for modal-trigger-field template.
 */
function template_preprocess_modal_trigger_field(array &$variables) {
  $elements = $variables['elements'];
  $block_css_class = 'modal-trigger';

  if (isset($elements['#item'])) {
    $item = $elements['#item'];

    $variables['attributes'] = new Attribute([
      'class' => [
        $block_css_class,
        _s360_modal_get_modal_trigger_modifier_css_classes($block_css_class, $item),
      ],
    ]);

    $variables['trigger'] = [];
    $variables['trigger']['label'] = $item->label;
    $variables['trigger']['attributes'] = new Attribute([]);
    $variables['trigger']['attributes']['aria-controls'] = $item->modal_id;
    $variables['trigger']['attributes']['data-js'] = 'modal-trigger';
  }
}

/**
 * Prepares variables for modal template.
 */
function template_preprocess_modal(array &$variables, $block_css_class = "modal") {
  $elements = $variables['elements'];

  if (isset($elements['#paragraph'])) {
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $elements['#paragraph'];

    if ($paragraph->hasField('field_modal_options')) {
      $field_modal_options = $paragraph->get('field_modal_options');
      $trigger_options = $field_modal_options->trigger_options;
      $display_options = $field_modal_options->display_options;

      $modal_options = $trigger_options;
      unset($modal_options['banish_text']);

      $modal_options['position'] = $display_options['position'];
      $modal_options['block_css_class'] = $block_css_class;

      $variables['attributes']['class'] = [
        $block_css_class,
        'pararaph--modal',
        _s360_modal_get_modal_modifier_css_classes($block_css_class, $display_options),
      ];

      $variables['attributes']['data-modal-settings'] = json_encode($modal_options);
      $variables['attributes']['id'] = _s360_modal_get_modal_id($paragraph->id());
      $variables['attributes']['role'] = 'dialog';
      $variables['attributes']['aria-hidden'] = 'true';
      $variables['attributes']['data-js'] = 'modal';

      // If there is banish text, create a button to pass into the template.
      if (!empty($trigger_options['banish_text'])) {
        $variables['modal_banish'] = [
          '#type' => 'html_tag',
          '#tag' => 'button',
          '#value' => $trigger_options['banish_text'],
          '#attributes' => [
            'class' => $block_css_class . '__banish-button',
            'data-js' => 'modal-banish-button',
            'title' => $trigger_options['banish_text'],
            'aria-label' => $trigger_options['banish_text'],
          ],
        ];
      }
    }

    $variables['modal_close_button'] = [];
    $variables['modal_close_button']['attributes'] = new Attribute([]);
    $variables['modal_close_button']['attributes']['data-js'] = 'modal-close-button';
    $variables['modal_close_button']['attributes']['class'] = $block_css_class . '__close-button';

    $variables['modal_footer'] = [];
    $variables['modal_footer']['attributes'] = new Attribute([]);
    $variables['modal_footer']['attributes']['class'] = $block_css_class . '__footer';
  }
}

/**
 * Returns a string of modal trigger modifier classes.
 *
 * @param string $block_css_class
 *   The block class for the modal-trigger.
 * @param \Drupal\s360_modal\Plugin\Field\FieldType\ModalTriggerFieldItem $item
 *   The entire modal-trigger object.
 *
 * @return string
 *   The modifier classes for the modal-trigger.
 */
function _s360_modal_get_modal_trigger_modifier_css_classes(string $block_css_class, ModalTriggerFieldItem $item) {
  $modifier_css_classes = [];

  $item_options = $item->options;

  if (isset($item_options['style'])) {
    $modifier_css_classes[] = $block_css_class . '--style-' . $item_options['style'];
  }

  return implode(' ', $modifier_css_classes);
}

/**
 * Returns a string of modal modifier classes.
 *
 * @param string $block_css_class
 *   The block class for the modal.
 * @param array $display_options
 *   An array of display options.
 *
 * @return string
 *   The modifier classes for the modal.
 */
function _s360_modal_get_modal_modifier_css_classes(string $block_css_class, array $display_options) {
  $modifier_css_classes = [$block_css_class . '--hidden'];

  if (!empty($display_options['style'])) {
    $modifier_css_classes[] = $block_css_class . '--style-' . $display_options['style'];
  }

  $modifier_css_classes[] = $block_css_class . '--position-' . $display_options['position'];

  return implode(' ', $modifier_css_classes);
}

/**
 * Create a unique modal id.
 *
 * @param int $id
 *   The id of the paragraph.
 *
 * @return string
 *   The new modal id.
 */
function _s360_modal_get_modal_id(int $id) {
  return 'modal-' . $id;
}
